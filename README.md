
## Sobre o projeto

Aplicação Laravel utilizando o conceito de Api  

## Packages de terceiros utilizados

Maatwebsite/Excel : https://github.com/maatwebsite/Laravel-Excel (Usado para realizar a importação de arquivos Excel).

## Considerações

Na pasta "postman" dentro do projeto consta o JSON com as collections de teste do Postman.
