<?php

namespace App\Enum;

abstract class PlanilhaStatus
{
    const FILA = 'fila';
    const PROCESSANDO = 'processando';
    const CONCLUIDO = 'concluido';
    const FALHA = 'falha';
}
