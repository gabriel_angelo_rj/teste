<?php

namespace App\Events;

//use App\Models\Planilha;
//use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
//use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
//use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PlanilhaImportada
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $planilha_id;

    public function planilha_id()
    {
        return $this->planilha_id;
    }

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($planilha_id)
    {
        $this->planilha_id = $planilha_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
