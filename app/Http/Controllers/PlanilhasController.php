<?php

namespace App\Http\Controllers;

use App\Enum\PlanilhaStatus;
use App\Services\PlanilhaService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Nette\NotImplementedException;

class PlanilhasController extends Controller
{
    private $planilhaService;

    public function __construct()
    {
        $this->planilhaService = App::make(PlanilhaService::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $planilhas = $this->planilhaService->getAll();

        return response()->json($planilhas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $planilha = $this->planilhaService->store($request->all());

            if(!$planilha)
                throw new \Exception('Ocorreu um erro ao tentar gravar a planilha com os resíduos.');

            return response()->json([
                'msg' => 'Planilha importada com sucesso, utilize o id gerado pela importação para verificar o status do processamento dos dados.',
                'planilha_id' => $planilha->id,
            ], 201);
        }catch (\Exception $exception){
            return response()->json([
                'msg' => $exception->getMessage(),
            ], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            return response()->json($this->planilhaService->findById($id));
        }catch (\Exception $exception){
            return response()->json([
                'msg' => $exception->getMessage(),
            ], 422);
        }
    }

    /**
     * @param string $orderBy
     * @param string $orderDirection
     * @param false $paginate
     * @param false $take
     * @param false $page
     * @return \Illuminate\Http\JsonResponse
     */
    public function showUnprocessedPlanilhas($orderBy = 'created_at', $orderDirection = 'desc', $paginate = false, $take = false, $page = false)
    {
        try {
            return response()->json($this->planilhaService->getPlanilhasByStatus(PlanilhaStatus::FALHA, $orderBy, $orderDirection, $paginate, $take, $page));
        }catch (\Exception $exception){
            return response()->json([
                'msg' => $exception->getMessage(),
            ], 422);
        }
    }
}
