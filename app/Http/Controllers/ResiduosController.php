<?php

namespace App\Http\Controllers;

use App\Services\ResiduoService;
use Facade\FlareClient\Http\Exceptions\NotFound;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class ResiduosController extends Controller
{
    private $residuoService;

    public function __construct()
    {
        $this->residuoService = App::make(ResiduoService::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json($this->residuoService->getAll());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            return response()->json($this->residuoService->findByID($id));
        }catch (\Exception $exception){
            response()->json([
                'msg' => $exception->getMessage(),
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $residuo = $this->residuoService->findById($id);

            if(!$residuo)
                throw new NotFound('O resíduo solicitado para alteração não foi encontrado.');

            if($this->residuoService->update($request->all(), $residuo))
                return response()->json(['msg' => 'Resíduo alterado com sucesso.'], 201);
        }catch (\Exception $exception){
            response()->json([
                'msg' => $exception->getMessage(),
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $residuo = $this->residuoService->findById($id);

            if(!$residuo)
                throw new NotFound('O resíduo solicitado para alteração não foi encontrado.');

            if($this->residuoService->delete($residuo))
                return response()->json(['msg' => 'Resíduo excluído com sucesso.']);
        }catch (\Exception $exception){
            response()->json([
                'msg' => $exception->getMessage(),
            ], 404);
        }
    }
}
