<?php

namespace App\Imports;

use App\Services\Repositories\Interfaces\ResiduoRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Maatwebsite\Excel\Concerns\ToCollection;

class ResiduosImport implements ToCollection
{
    private $residuoRepository;
    private $planilha_id;

    public function __construct($planilha_id)
    {
        $this->residuoRepository = App::make(ResiduoRepositoryInterface::class) ;
        $this->planilha_id = $planilha_id;
    }

    public function collection(Collection $collection)
    {
        foreach ($collection as $row) {
            if($row[1] !== null && trim($row[1]) !== 'Nome Comum do Resíduo'){
                $residuo = [
                    'planilha_id' => $this->planilha_id,
                    'nome_comum_residuo' => $row[1],
                    'tipo_de_residuo' => $row[2],
                    'categoria' => $row[3],
                    'tecnologia_de_tratamento' => $row[4],
                    'classe' => $row[5],
                    'unidade_de_medida' => $row[6],
                    'peso' => $row[7],
                ];

                $this->residuoRepository->create($residuo);
            }
        }
    }
}
