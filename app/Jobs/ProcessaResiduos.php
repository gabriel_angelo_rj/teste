<?php

namespace App\Jobs;

use App\Enum\PlanilhaStatus;
use App\Exceptions\ProcessamentoResiduosException;
use App\Imports\ResiduosImport;
use App\Services\Repositories\Interfaces\PlanilhaRepositoryInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class ProcessaResiduos implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $planilhaRepository;
    private $planilha_id;
    private $planilha;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($planilha_id)
    {
        $this->planilhaRepository = App::make(PlanilhaRepositoryInterface::class);
        $this->planilha_id = $planilha_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $this->iniciaProcessamentoDeResiduos();
        } catch (FileNotFoundException $fileNotFoundException) {
            $this->logaFalhaNoProcessamentoDeResiduos($fileNotFoundException->getMessage());
        }catch (ProcessamentoResiduosException $processamentoResiduosException){
            $this->logaFalhaNoProcessamentoDeResiduos($processamentoResiduosException->getMessage());
        }
    }

    /**
     * @throws FileNotFoundException|ProcessamentoResiduosException
     */
    private function iniciaProcessamentoDeResiduos(){
        $this->planilha = $this->planilhaRepository->findByID($this->planilha_id);

        $this->planilhaRepository->update($this->planilha, ['status' => PlanilhaStatus::PROCESSANDO]);

        if(Storage::exists('planilhas/'.$this->planilha->nome_arquivo) == false)
            throw new FileNotFoundException('Arquivo para processamento dos dados não encontrado.');

        try {
            Excel::import(new ResiduosImport($this->planilha->id), 'planilhas/'.$this->planilha->nome_arquivo);

            $this->finalizaProcessamentoDaPlanilhaDeResiduos();
        }catch (\Exception $exception){
            throw new ProcessamentoResiduosException($exception->getMessage());
        }
    }

    private function logaFalhaNoProcessamentoDeResiduos($error_message){
        $this->planilhaRepository->update($this->planilha, ['status' => PlanilhaStatus::FALHA, 'texto_falha' => $error_message]);
    }

    private function finalizaProcessamentoDaPlanilhaDeResiduos(){
        $this->planilhaRepository->update($this->planilha, ['status' => PlanilhaStatus::CONCLUIDO]);
    }
}
