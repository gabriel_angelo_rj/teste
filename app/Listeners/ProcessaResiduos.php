<?php

namespace App\Listeners;

use App\Events\PlanilhaImportada;
use App\Jobs\ProcessaResiduos as ProcessaResiduosJob;
//use Illuminate\Contracts\Queue\ShouldQueue;
//use Illuminate\Queue\InteractsWithQueue;

class ProcessaResiduos
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PlanilhaImportada  $event
     * @return void
     */
    public function handle(PlanilhaImportada $event)
    {
        ProcessaResiduosJob::dispatch($event->planilha_id());
    }
}
