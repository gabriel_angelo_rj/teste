<?php

namespace App\Models;

use App\Enum\PlanilhaStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Planilha extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['id'];

    protected $attributes = [
        'users_id' => null,
        'texto_falha' => null,
        'status' => PlanilhaStatus::PROCESSANDO
    ];

    public function getStatusAttribute(){
        switch ($this->attributes['status']){
            case PlanilhaStatus::FILA:
                return 'Aguardando processamento';
            case PlanilhaStatus::PROCESSANDO:
                return 'Processando';
            case PlanilhaStatus::CONCLUIDO:
                return 'Processamento concluído';
            case PlanilhaStatus::FALHA:
                return 'Falha no processamento';
        }
    }
}
