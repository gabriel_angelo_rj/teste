<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Residuo extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['id'];

    public function planilha(){
        return $this->belongsTo(Planilha::class, 'planilha_id', 'id');
    }
}
