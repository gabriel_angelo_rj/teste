<?php

namespace App\Providers;

use App\Services\Repositories\BaseRepository;
use App\Services\Repositories\Interfaces\BaseRepositoryInterface;
use App\Services\Repositories\Interfaces\PlanilhaRepositoryInterface;
use App\Services\Repositories\Interfaces\ResiduoRepositoryInterface;
use App\Services\Repositories\PlanilhaRepository;
use App\Services\Repositories\ResiduoRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BaseRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(PlanilhaRepositoryInterface::class, PlanilhaRepository::class);
        $this->app->bind(ResiduoRepositoryInterface::class, ResiduoRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
