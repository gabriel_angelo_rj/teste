<?php

namespace App\Services;

use App\Events\PlanilhaImportada;
use App\Services\Repositories\Interfaces\PlanilhaRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PlanilhaService
{
    private $planilhaRepository;

    public function __construct(PlanilhaRepositoryInterface $planilhaRepository)
    {
        $this->planilhaRepository = $planilhaRepository;
    }

    public function getAll(){
        return $this->planilhaRepository->getAll();
    }

    public function findById($id){
        return $this->planilhaRepository->findByID($id);
    }

    /**
     * @throws \Exception
     */
    public function store(array $data){
        DB::beginTransaction();

        $file = $data['arquivo'];
        $nome_original_arquivo = $file->getClientOriginalName();
        $nome_arquivo = now()->timestamp . '.' . $file->getClientOriginalExtension();

        try {
            $this->upload($file, $nome_arquivo);

            $planilha = $this->planilhaRepository->create(['nome_arquivo' => $nome_arquivo, 'nome_original_arquivo' => $nome_original_arquivo]);

            DB::commit();

            event(new PlanilhaImportada($planilha->id));

            return $planilha;
        }catch (\Exception $exception){
            DB::rollBack();

            $this->rollbackFileUpload($nome_arquivo);

            throw $exception;
        }
    }

    private function upload($file, $nome_arquivo){
        return $file->storeAs('planilhas', $nome_arquivo);
    }

    private function rollbackFileUpload($nome_arquivo){
        $path = "planilhas/$nome_arquivo";

        if(Storage::exists($path))
            Storage::delete($path);
    }

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function getPlanilhasByStatus($status, $orderBy = 'created_at', $orderDirection = 'desc', $paginate = false, $take = 5, $page = 1){
        return $this->planilhaRepository->getPlanilhasByStatus($status, $orderBy, $orderDirection, $paginate, $take, $page);
    }
}
