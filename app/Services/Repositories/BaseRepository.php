<?php

namespace App\Services\Repositories;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Builder as EloquentQueryBuilder;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder as QueryBuilder;

class BaseRepository implements Interfaces\BaseRepositoryInterface
{

    /**
     * Model para repositório.
     *
     * @var string
     */
    protected $modelClass;

    /**
     * @param EloquentQueryBuilder|QueryBuilder $query
     * @param bool $paginate | default: false
     *
     * @param int $take
     * @param int $page
     * @return EloquentCollection|array
     * @throws BindingResolutionException
     */
    protected function doQuery($query = null, bool $paginate = false, $take = 5, $page = 1)
    {
        if (is_null($query)) {
            $query = $this->newQuery();
        }

        if ($paginate == true) {
            if ($take > 0 || false !== $take) {
                $paginator = $query->paginate($take);
                $total_pages = $paginator->lastPage();
                $result_collection = $query->forPage($page, $take)->get();

                return [
                    'total_pages' => $total_pages,
                    'result' => $result_collection
                ];
            }else
                throw new \InvalidArgumentException('Quando o parâmetro $paginate for true se torna obrigatório que $take e $page sejam maiores que 0 (zero).');
        }

        return $query->get();
    }

    /**
     * @return EloquentQueryBuilder|QueryBuilder
     * @throws BindingResolutionException
     */
    protected function newQuery()
    {
        return app()->make($this->modelClass)->newQuery();
    }

    /**
     * Returns all records.
     * If $take is false then brings all records
     * If $paginate is true returns Paginator instance.
     *
     * @param bool $paginate
     *
     * @param int $take
     * @param int $page
     * @return EloquentCollection|array
     * @throws BindingResolutionException
     */
    public function getAll(bool $paginate = false, $take = 5, $page = 1)
    {
        return $this->doQuery(null, $paginate, $take, $page);
    }

    /**
     * Retorna um registro solicitado pelo parametro $id.
     * Se $fail for true então será acionado a exceção ModelNotFoundException caso não seja encontrado o registro.
     *
     * @param int $id
     * @param bool $fail | default: true
     *
     * @return Model
     * @throws BindingResolutionException
     */
    public function findByID($id, bool $fail = true) : ?Model
    {
        if ($fail) {
            return $this->newQuery()->findOrFail($id);
        }

        return $this->newQuery()->find($id);
    }
}
