<?php

namespace App\Services\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface BaseRepositoryInterface
{
    /**
     * Retorna todos os registros.
     * Se o parametro $paginate for false então serão retornados todos os registros da base de dados.
     * Se $paginate for true informe $take (quantidade de registros por pagina)
     * e informe $page (pagina atual).
     *
     * @param bool $paginate | default: false
     * @param int $take | default: 5
     * @param int $page | default: 1
     *
     * @return array|Collection |
     * No caso de $paginate for false será retornado a collection com os dados solicitados, caso seja true
     * então será retornado um array simples com #total_pages (total de páginas) e #retorno(collection dos dados).
     */
    public function getAll(bool $paginate = false, $take = 5, $page = 1);

    /**
     * Retorna o registro solicitado pelo parametro $id.
     * Se $fail for true então acionará a exceção ModelNotFoundException no caso de não encontrar nenhum registro
     * com o $id solicitado.
     *
     * @param int $id
     * @param bool $fail | default: true
     *
     * @return Model
     */
    public function findByID(int $id, bool $fail = true): ?Model;
}
