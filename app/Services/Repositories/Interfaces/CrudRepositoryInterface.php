<?php

namespace App\Services\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Model;

interface CrudRepositoryInterface
{
    /**
     * Cria um objeto Model com as informações do parâmetro $data.
     *
     * @param array $data
     *
     * @return Model
     */
    public function create(array $data = []): Model;

    /**
     * Atualiza a model enviada como parâmetro com os dados do array de $data.
     *
     * @param Model $model
     * @param array $data
     *
     * @return bool
     */
    public function update(Model $model, array $data = []): bool;

    /**
     * Executa o método de salvamento do modelo, permitindo implementação anterior da lógica de negócio.
     *
     * @param Model $model
     *
     * @return bool
     */
    public function save(Model $model): bool;

    /**
     * Executa o método de exclusão do modelo, permitindo implementação anterior da lógica de negócio.
     *
     * @param Model $model
     *
     * @return bool
     */
    public function delete(Model $model): bool;

    /**
     * Fabrica o objeto Model por meio dos dados enviados pelo array $data.
     *
     * @param array $data
     *
     * @return Model
     */
    public function factory(array $data = []): Model;
}
