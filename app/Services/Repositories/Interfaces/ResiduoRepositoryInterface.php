<?php

namespace App\Services\Repositories\Interfaces;

interface ResiduoRepositoryInterface extends BaseRepositoryInterface, CrudRepositoryInterface
{
}
