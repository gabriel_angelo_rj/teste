<?php

namespace App\Services\Repositories;

use App\Models\Planilha;
use App\Services\Repositories\Interfaces\PlanilhaRepositoryInterface;
use App\Traits\BasicCrudMethods;

class PlanilhaRepository extends BaseRepository implements PlanilhaRepositoryInterface
{
    use BasicCrudMethods;

    protected $modelClass = Planilha::class;

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function getPlanilhasByStatus($status, $orderBy = 'created_at', $orderDirection = 'desc', $paginate = false, $take = 5, $page = 1){
        $query = $this->newQuery();

        $query->where('status', '=', $status)
            ->orderBy($orderBy, $orderDirection);

        return $this->doQuery($query, $paginate, $take, $page);
    }
}
