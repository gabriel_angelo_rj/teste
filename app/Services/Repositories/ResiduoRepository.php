<?php

namespace App\Services\Repositories;

use App\Models\Residuo;
use App\Services\Repositories\Interfaces\ResiduoRepositoryInterface;
use App\Traits\BasicCrudMethods;

class ResiduoRepository extends BaseRepository implements ResiduoRepositoryInterface
{
    use BasicCrudMethods;

    protected $modelClass = Residuo::class;
}
