<?php

namespace App\Services;

use App\Services\Repositories\Interfaces\ResiduoRepositoryInterface;

class ResiduoService
{
    private $residuoRepository;

    public function __construct(ResiduoRepositoryInterface $residuoRepository)
    {
        $this->residuoRepository = $residuoRepository;
    }

    public function getAll(){
        return $this->residuoRepository->getAll();
    }

    public function findByID($id): ?\Illuminate\Database\Eloquent\Model
    {
        return $this->residuoRepository->findByID($id);
    }

    public function update(array $data, $residuo): bool
    {
        return $this->residuoRepository->update($residuo, $data);
    }

    public function delete($residuo): bool
    {
        return $this->residuoRepository->delete($residuo);
    }
}
