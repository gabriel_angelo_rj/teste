<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;

trait BasicCrudMethods
{
    /**
     *
     *
     * @param Model $model
     * @param array $data
     *
     * @return bool
     */
    public function update(Model $model, array $data = []): bool
    {
        $this->setModelData($model, $data);

        return $this->save($model);
    }

    /**
     *
     *
     * @param array $data
     *
     * @return Model
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function factory(array $data = []): Model
    {
        $model = $this->newQuery()->getModel()->newInstance();

        $this->setModelData($model, $data);

        return $model;
    }

    /**
     * @param Model $model
     * @param array $data
     */
    protected function setModelData(Model $model, array $data)
    {
        $model->fill($data);
    }

    /**
     * @param array $data
     *
     * @return Model
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function create(array $data = []): Model
    {
        $model = $this->factory($data);

        $this->save($model);

        return $model;
    }

    /**
     * .
     *
     * @param Model $model
     *
     * @return bool
     */
    public function save(Model $model): bool
    {
        return $model->save();
    }

    /**
     * .
     *
     * @param Model $model
     *
     * @return bool
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }
}
