<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanilhasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planilhas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('users_id')->nullable()->constrained('users');
            $table->string('nome_original_arquivo', 150);
            $table->string('nome_arquivo', 150);
            $table->enum('status', ['fila', 'processando', 'concluido', 'falha']);
            $table->text('texto_falha')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planilhas');
    }
}
