<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResiduosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('residuos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('planilha_id')->constrained('planilhas');
            $table->string('nome_comum_residuo', 150);
            $table->string('tipo_de_residuo', 150);
            $table->string('categoria', 80);
            $table->string('tecnologia_de_tratamento', 80);
            $table->string('classe', 10);
            $table->string('unidade_de_medida', 5);
            $table->decimal('peso',8,3);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('residuos');
    }
}
