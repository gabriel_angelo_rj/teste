<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/planilhas', ['App\Http\Controllers\PlanilhasController', 'index']);
Route::post('/planilhas', ['App\Http\Controllers\PlanilhasController', 'store']);
Route::get('/planilhas/{id}', ['App\Http\Controllers\PlanilhasController', 'show']);
Route::get('/planilhas/status/unprocessed/{orderBy?}/{orderDirection?}/{paginate?}/{take?}/{page?}', ['App\Http\Controllers\PlanilhasController', 'showUnprocessedPlanilhas']);

Route::get('/residuos', ['App\Http\Controllers\ResiduosController', 'index']);
Route::get('/residuos/{id}', ['App\Http\Controllers\ResiduosController', 'show']);
Route::put('/residuos/{id}', ['App\Http\Controllers\ResiduosController', 'update']);
Route::delete('/residuos/{id}', ['App\Http\Controllers\ResiduosController', 'destroy']);

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/
